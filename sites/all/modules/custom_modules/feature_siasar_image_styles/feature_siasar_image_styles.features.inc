<?php
/**
 * @file
 * feature_siasar_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function feature_siasar_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: full_hd.
  $styles['full_hd'] = array(
    'label' => 'Full HD',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1920,
          'height' => 1080,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid_main.
  $styles['grid_main'] = array(
    'label' => 'grid_main',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 320,
          'height' => 180,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: hd.
  $styles['hd'] = array(
    'label' => 'HD',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1280,
          'height' => 720,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: minibox.
  $styles['minibox'] = array(
    'label' => 'MInibox',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 20,
          'height' => 20,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: photo_thumbnail.
  $styles['photo_thumbnail'] = array(
    'label' => 'Photo thumbnail',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 155,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

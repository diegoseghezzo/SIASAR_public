<?php
/**
 * @file
 * siasar_content_country_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function siasar_content_country_profile_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|node|country_profile|default';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact',
    'weight' => '7',
    'children' => array(
      0 => 'field_address',
      1 => 'field_contact_person',
      2 => 'field_email',
      3 => 'field_phone',
      4 => 'field_organization',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Contact',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-contact group-country-datasheet',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_contact|node|country_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_contact|node|country_profile|form';
  $field_group->group_name = 'group_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact',
    'weight' => '6',
    'children' => array(
      0 => 'field_address',
      1 => 'field_contact_person',
      2 => 'field_phone',
      3 => 'field_organization',
      4 => 'field_email',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_contact|node|country_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_download_links|node|country_profile|form';
  $field_group->group_name = 'group_download_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Descarga de Ficheros',
    'weight' => '35',
    'children' => array(
      0 => 'field_com_download_links',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Descarga de Ficheros',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_download_links|node|country_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile|node|country_profile|default';
  $field_group->group_name = 'group_profile';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '1',
    'children' => array(
      0 => 'field_currency',
      1 => 'field_area',
      2 => 'field_capital_city',
      3 => 'field_population_density',
      4 => 'field_language',
      5 => 'field_human_development_index',
      6 => 'field_population',
      7 => 'field_government',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Profile',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-profile group-country-datasheet',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_profile|node|country_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile|node|country_profile|form';
  $field_group->group_name = 'group_profile';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Profile',
    'weight' => '4',
    'children' => array(
      0 => 'field_currency',
      1 => 'field_human_development_index',
      2 => 'field_language',
      3 => 'field_government',
      4 => 'field_area',
      5 => 'field_capital_city',
      6 => 'field_population_density',
      7 => 'field_population',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Profile',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_profile|node|country_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reports_links|node|country_profile|form';
  $field_group->group_name = 'group_reports_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reportes Completos',
    'weight' => '14',
    'children' => array(
      0 => 'field_com_report_links',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Reportes Completos',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_reports_links|node|country_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reports|node|country_profile|default';
  $field_group->group_name = 'group_reports';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reports',
    'weight' => '6',
    'children' => array(
      0 => 'field_com_download_links',
      1 => 'field_com_report_links',
      2 => 'field_link_country_report_data',
    ),
    'format_type' => 'html5',
    'format_settings' => array(
      'label' => 'Reports',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-reports',
        'wrapper' => 'aside',
      ),
    ),
  );
  $field_groups['group_reports|node|country_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reports|node|country_profile|form';
  $field_group->group_name = 'group_reports';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reports',
    'weight' => '13',
    'children' => array(
      0 => 'field_link_country_report_data',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-reports field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_reports|node|country_profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rural|node|country_profile|default';
  $field_group->group_name = 'group_rural';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Rural Data',
    'weight' => '2',
    'children' => array(
      0 => 'field_n_est_rural_providers',
      1 => 'field_n_estimated_rural_systems',
      2 => 'field_n_rural_communities',
      3 => 'field_n_rural_tech_assistance_pr',
      4 => 'field_rural_popullation',
      5 => 'field_housing',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Rural Data',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-contact group-country-datasheet',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_rural|node|country_profile|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rural|node|country_profile|form';
  $field_group->group_name = 'group_rural';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Rural Data',
    'weight' => '5',
    'children' => array(
      0 => 'field_housing',
      1 => 'field_n_rural_communities',
      2 => 'field_n_est_rural_providers',
      3 => 'field_n_estimated_rural_systems',
      4 => 'field_rural_popullation',
      5 => 'field_n_rural_tech_assistance_pr',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Rural Data',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_rural|node|country_profile|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact');
  t('Descarga de Ficheros');
  t('Profile');
  t('Reportes Completos');
  t('Reports');
  t('Rural Data');

  return $field_groups;
}

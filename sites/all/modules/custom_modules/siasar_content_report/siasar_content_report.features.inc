<?php
/**
 * @file
 * siasar_content_report.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function siasar_content_report_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function siasar_content_report_node_info() {
  $items = array(
    'report' => array(
      'name' => t('Report'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Country name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

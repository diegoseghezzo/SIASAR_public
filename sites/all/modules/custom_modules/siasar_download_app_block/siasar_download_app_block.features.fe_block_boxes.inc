<?php
/**
 * @file
 * siasar_download_app_block.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function siasar_download_app_block_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Download the app';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'download_app';
  $fe_block_boxes->body = '<strong>SIASApp</strong> es una aplicación para el levantamiento de los cuestionarios de SIASAR de manera ágil y fiable.

<div class="links-to-apps"><a href="https://play.google.com/store/apps/details?id=app.siasar.org" target="_blank" class="android">Download App</a></div>';

  $export['download_app'] = $fe_block_boxes;

  return $export;
}

<?php
/**
 * @file
 * siasar_social_media_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function siasar_social_media_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-social-media_blog:http://www.siasar.org/blog/.
  $menu_links['menu-social-media_blog:http://www.siasar.org/blog/'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'http://www.siasar.org/blog/',
    'router_path' => '',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(
        'id' => 'wordpress',
        'target' => '_blank',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-media_blog:http://www.siasar.org/blog/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-social-media_facebook:https://www.facebook.com/SIASARorg/.
  $menu_links['menu-social-media_facebook:https://www.facebook.com/SIASARorg/'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://www.facebook.com/SIASARorg/',
    'router_path' => '',
    'link_title' => 'Facebook',
    'options' => array(
      'attributes' => array(
        'id' => 'facebook',
        'target' => '_blank',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-media_facebook:https://www.facebook.com/SIASARorg/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-social-media_linkedin:https://www.linkedin.com/company-beta/10253268/.
  $menu_links['menu-social-media_linkedin:https://www.linkedin.com/company-beta/10253268/'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://www.linkedin.com/company-beta/10253268/',
    'router_path' => '',
    'link_title' => 'LinkedIn',
    'options' => array(
      'attributes' => array(
        'id' => 'linkedin',
        'target' => '_blank',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-media_linkedin:https://www.linkedin.com/company-beta/10253268/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-social-media_twitter:https://twitter.com/siasar_org.
  $menu_links['menu-social-media_twitter:https://twitter.com/siasar_org'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://twitter.com/siasar_org',
    'router_path' => '',
    'link_title' => 'Twitter',
    'options' => array(
      'attributes' => array(
        'id' => 'twitter',
        'target' => '_blank',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-social-media_twitter:https://twitter.com/siasar_org',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');
  t('Facebook');
  t('LinkedIn');
  t('Twitter');

  return $menu_links;
}

(function ($) {
  $(window).load(function() {
    initSlider();


  }); // end window.load
  function initSlider() {
    if (typeof $.fn.slick == 'undefined') return;

    var $slider = $('#block-views-success-stories-block .content');

    $slider.slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      draggable: true,
      useCSS: false,
      useTransform: true,
      adaptiveHeight: true,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 1
          }
        }
      ],
    });
  }
})(jQuery);

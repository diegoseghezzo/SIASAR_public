<?php
/**
 * @file
 * siasar_views_success_stories.features.inc
 */

/**
 * Implements hook_views_api().
 */
function siasar_views_success_stories_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

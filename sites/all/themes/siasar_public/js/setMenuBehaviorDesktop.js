
(function ($) {


  $(document).ready(function () {
    var $mainMenu = $('#block-system-main-menu');
    var $links = $mainMenu.find('> ul > li');
    var timerHoverMenu;

    $links.on('mouseenter tap', addHoverClassToLink);

    $links.on('mouseleave', removeHoverClassToLink);

    function addHoverClassToLink(e) {
      if (timerHoverMenu) clearTimeout(timerHoverMenu);
      console.log('enter');

      if (e.type == 'tap' && !this.classList.contains('tap')) {
        $links.removeClass('tapped');
        this.classList.add('tapped');
        e.preventDefault();
      }
      if (!this.classList.contains('hover')) {
        this.classList.add('hover');
      }
    }

    function removeHoverClassToLink(e) {
      console.log('leave');
      timerHoverMenu = setTimeout(function () {
        e.currentTarget.classList.remove('hover');
      }, 1000);
    }
  });


})(jQuery);

<?php
/*
  Preprocess
*/


/*
  Implements theme preprocess
*/
function siasar_public_preprocess(&$vars, $hook) {
  $vars['appletouchicon'] = _siasar_public_add_apple_touch_icons();
}

/*
  Helper function for them preprocess.
  Returns header HTML to add Apple touch icons to the page.
*/
function _siasar_public_add_apple_touch_icons() {
  global $theme, $base_url;
  $path = drupal_get_path('theme', $theme);
  $path_mothership = drupal_get_path('theme', 'mothership');

  //For third-generation iPad with high-resolution Retina display
  $appletouchicon = '<link rel="apple-touch-icon" sizes="144x144" href="' . $base_url .'/'. $path . '/apple-icon-144x144.png">';
  //For iPhone with high-resolution Retina display
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="114x114" href="' . $base_url .'/'. $path . '/apple-icon-114x114.png">'. "\n";
  //For first- and second-generation iPad:
  $appletouchicon .= '<link rel="apple-touch-icon" sizes="72x72" href="' . $base_url .'/'.  $path . '/apple-icon-72x72.png">' . "\n";
  //For non-Retina iPhone, iPod Touch, and Android 2.1+ devices
  $appletouchicon .=  '<link rel="apple-touch-icon" href="' . $base_url .'/'.  $path . '/apple-icon.png">' . "\n";
  $appletouchicon .=  '<link rel="apple-touch-startup-image" href="' . $base_url .'/'.  $path . '/apple-icon.png">' . "\n";

  return $appletouchicon;
}


function siasar_public_links__locale_block(&$variables) {
  // the global $language variable tells you what the current language is
  global $language;

  // an array of list items
  $items = array();
  foreach($variables['links'] as $lang => $info) {
    $name     = substr($info['language']->language, 0, 2);
    $href     = isset($info['href']) ? $info['href'] : '';
    $li_classes   = array('list-item-class');
    // if the global language is that of this item's language, add the active class
    if($lang === $language->language){
          $li_classes[] = 'active';
    }
    $link_classes = array('link-class1', 'link-class2');
    $options = array('attributes' => array(
      'class'    => $link_classes),
      'language' => $info['language'],
      'html'     => true
    );
    $link = l($name, $href, $options);

    // display only translated links
    if ($href) $items[] = array('data' => $link, 'class' => $li_classes);
  }

  // output
  $attributes = array('class' => array('my-list'));
  $output = theme_item_list(
    array(
      'items' => $items,
      'title' => '',
      'type'  => 'ul',
      'attributes' => $attributes
    )
  );
  return $output;
}

function siasar_public_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  return $output;
}

/**
 * Implements template_preprocess_node().
 */
function siasar_public_preprocess_node(&$variables, $hook) {
  $path_siasar_theme = drupal_get_path('theme', 'siasar_public');
  include_once './' . $path_siasar_theme . '/includes/template.helpers.php';

  $variables['header'] = _siasar_public_main_photo_process($variables);
  $variables['dashboard_url'] = _siasar_public_get_dashboard_path($variables);
}


/**
 * Implements template_preprocess_field().
 */
function siasar_public_preprocess_field(&$variables, $hook) {
  if($variables['element']['#field_name'] !== 'field_link_country_report_data') return;

  $element = &$variables['element'];
  $country_name = $element['#object']->title;
  $link_string = t('See @country report', array('@country' => $country_name));
  $link = &$element['#items'][0]['entity']->field_link_report[LANGUAGE_NONE][0];
  $link_path = $link['url'];
  $link_title = $link['title'];

  $link_options = array(
    'attributes' => array(
      'title' => $link_title,
      'class' => 'button external',
      'target' => '_blank',
    )
  );

  $variables['link_to_report'] = l($link_string, $link_path, $link_options);
}

/**
 * Implements field_group_build_pre_render_alter().
 */
function siasar_public_field_group_build_pre_render_alter(&$element) {
  extract($element);

  if (!isset($group_reports)) return;

  $link_to_all_reports = array(
    '#markup' => l(t('See all reports'), 'reports'),
    '#weight' => 100,
  );

  $element['group_reports']['link_to_all_reports'] = $link_to_all_reports;
}

function siasar_public_file_formatter_table($variables) {
  $header = array(t('Attachment'), t('Type'), t('Updated'));
  $rows = array();
  foreach ($variables['items'] as $delta => $item) {
      $XXCUSTOMfilesize = format_size($item['filesize']);
    $rows[] = array(
      theme('file_link', array('file' => (object) $item)),
      $item['filemime'],
      format_date($item['timestamp'], $type = 'custom', 'j/n/Y'),
      );
  }

  return empty($rows) ? '' : theme('table', array('header' => $header, 'rows' => $rows));
}
